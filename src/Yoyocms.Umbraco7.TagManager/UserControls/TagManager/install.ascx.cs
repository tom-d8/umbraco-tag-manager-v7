﻿using System;
using System.Xml;

namespace Yoyocms.Umbraco7.TagManager.UserControls.TagManager
{
    public partial class install : System.Web.UI.UserControl
    {
        private TagManagerAPIController tmapic = new TagManagerAPIController();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Update failed to /umbraco/config/Lang/en.xml file
            if (!xmlFileUpdates.updateEnXML())
            {
                error.Visible = true;
                return;
            }

            //Add tag manager access for admin user
            if (tmapic.checkUserTable() == -1)
            {
                string sql3 = "INSERT INTO umbracoUser2App([user],[app]) VALUES(0,'TagManager');";

                try
                {
                    tmapic.AddDbEntry(sql3);
                }
                catch { }
            }

            //Everything worked fine - yay !
            done.Visible = true;
        }
    }

    public class xmlFileUpdates
    {
        //Add to /umbraco/config/lang/en.xml
        public static bool updateEnXML()
        {
            try
            {
                string filePath = System.Web.HttpContext.Current.Server.MapPath("~/umbraco/config/lang/en.xml");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);
                var testForNode = xmlDoc.SelectNodes("/language/area[@alias = 'sections']/key[@alias = 'TagManager']");
                if (testForNode.Count == 0)
                {
                    //Create a new XML <key> node
                    XmlElement keyNode = xmlDoc.CreateElement("key");
                    keyNode.SetAttribute("alias", "TagManager");
                    keyNode.InnerText = "Tag Manager";

                    //Add <key> to file and save.
                    xmlDoc.SelectSingleNode("/language/area[@alias='sections']").AppendChild(keyNode);
                    xmlDoc.Save(filePath);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}