﻿namespace Yoyocms.Umbraco7.TagManager.Models
{
    public class TaggedDocument
    {
        public string DocumentName { get; set; }

        public int DocumentId { get; set; }

        public string DocumentURL { get; set; }
    }
}