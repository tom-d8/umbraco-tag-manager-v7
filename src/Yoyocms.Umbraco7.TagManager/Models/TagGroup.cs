﻿using Umbraco.Core.Persistence;

namespace Yoyocms.Umbraco7.TagManager.Models
{
    [TableName("cmsTags")]
    public class TagGroup
    {
        public string group { get; set; }
    }
}