﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using umbraco.businesslogic;
using umbraco.interfaces;

namespace umbSite.App_Code
{
    [Application("peopleTree", "People", "icon-battery-low", 15)]
    public class PeopleApplication : IApplication { }
}